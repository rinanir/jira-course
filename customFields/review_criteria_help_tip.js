<script type="text/javascript">
    function reviewCriteriaFieldHelp() {
        var listenersDiv = document.getElementById("reviewCriteriaFieldHelp");
        if (listenersDiv.style.display == 'none') {
            listenersDiv.style.display = '';
        } else {
            listenersDiv.style.display='none';
        }
    }
    </script>
    <a href="#"  onclick="reviewCriteriaFieldHelp(); return false;"><span class="aui-icon aui-icon-small aui-iconfont-help"></span></a>
    <div id="reviewCriteriaFieldHelp" style="display:none">
    The review criteria should be detailed, quantifiable, and answer: what will be reviewed, what mechanisms will be used for review, and who are the stake=holders that should be involved in the review. It is very important that the provided review criteria will be as clear as possible.
</div>