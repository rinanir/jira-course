import com.atlassian.jira.component.ComponentAccessor

def constantsManager = ComponentAccessor.getConstantsManager()

def priority = constantsManager.getPriorities().findByName("Highest")
issueInputParameters.setPriorityId(priority.id)